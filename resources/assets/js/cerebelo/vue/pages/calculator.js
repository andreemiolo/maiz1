// Vue.component('services-item', {
//     props: ['service'],
//     template: '<li v-on:click="toggleActive(service)" v-bind:class="{\'active\': service.active}">' +
//     '{{service.name}} <span>{{service.price | currency}}</span>' +
//     '</li>'
// });

function addCalculator(id) {
    var lineDetails = getLine(id);
    calculators.push({
        id: id,
        calculator: new Vue({
            el: '#' + id,
            delimiters: ["((", "))"],
            methods: {
                getQuantity: function (object) {
                    var quantity = 0;

                    if (object.hasOwnProperty('quantity')) {
                        quantity = object.quantity;
                    } else {
                        if (object.type == 0) {
                            quantity = this.lineDetails.kilo;
                        } else {
                            quantity = this.lineDetails.meters;
                        }
                    }

                    return quantity;
                },
                calc: function (object) {
                    var quantity = this.getQuantity(object);

                    floatingBox.updateData();
                    return ((object.price * quantity).toFixed(2) * 1.0);
                },
                call: function (f) {
                    var a = null;
                    eval("a = this." + f + "();");
                    a = a * 1.0;
                    return (a.toFixed(2) * 1.0);
                },
                getAccessoryIndex: function (id) {
                    for (var i in this.accessories) {
                        if (this.accessories[i].id == id) {
                            return i;
                        }
                    }

                    return null;
                },

                calcTotalPrice: function (items) {
                    var total = 0.0;
                    for (var i in items) {
                        if (items.hasOwnProperty(i)) {
                            total += items[i].price;
                        }
                    }
                    return total;
                },
                calcTotalQuantity: function (items) {
                    var total = 0.0;
                    for (var i in items) {
                        if (items.hasOwnProperty(i)) {
                            total += this.getQuantity(items[i]);
                        }
                    }
                    return total;
                },

                calcTotal: function (type) {
                    var total = 0.0;
                    for (var i = 0; i < type.length; i++) {
                        total += (1.0 * type[i].price) * (1.0 * this.getQuantity(type[i]));
                    }
                    return total;
                },

                calcTotalAccessories: function () {
                    return this.calcTotal(this.accessories);
                },
                calcTotalDeliveries: function () {
                    return this.calcTotal(this.deliveries);
                },

                calcTotalTotal: function () {
                    return (
                    (
                        this.calcTotal(this.products) +
                        this.calcTotalDeliveries() +
                        this.calcCommissionTotal() +
                        this.calcTotalAccessories() +
                        this.calcTaxesTotal()
                    ).toFixed(2) * 1.0);
                },

                calcVariableTotal: function () {
                    return this.calcTotalTotal() - this.calc(this.products[this.products.length - 2]);
                },
                calcSellTotal: function () {
                    return this.getQuantity(this.products[0]) * this.calculated.kiloPrice.value;
                },
                calcMarginTotal: function () {
                    return this.calcSellTotal() - this.calcVariableTotal();
                },
                calcMarginPercent: function () {
                    return this.calcMarginTotal() / this.calcSellTotal();
                },
                calcCommissionTotal: function () {
                    //todo : ovo e galinha
                    // return this.calcTotalTotal() * (this.calculated.commission.percent / 100);
                    return this.calcSellTotal() * (this.calculated.commission.percent / 100);
                },
                calcTaxesTotal: function () {
                    return ((this.calcSellTotal() * (this.calculated.taxes.rate / 100)) * (this.calculated.taxes.percent / 100));
                },
                calcProfitTotal: function () {
                    return this.calcSellTotal() - this.calcTotalTotal();
                },
                calcProfitPercent: function () {
                    var value = (
                    (
                        (1 - (this.calcTotalTotal() / this.calcSellTotal())) * 100
                    ).toFixed(3) * 1.0);
                    if (isNaN(value) && !isFinite(value)) {
                        return 0;
                    }
                    return value;
                }
            },
            data: {
                lineDetails: lineDetails,
                linePrice: lineDetails.price,
                lineKilo: lineDetails.kilo,
                lineMeters: lineDetails.meters,
                kiloPrice: constants.kiloPrice,
                calculated: {},
                products: [
                    {
                        name: "Alumínio",
                        price: 13.00,
                        edited: false,
                        calculated: false,
                        type: 0
                    },
                    {
                        name: "Frete",
                        price: 0.50,
                        edited: false,
                        calculated: false,
                        type: 0
                    },
                    {
                        name: "Anodização / Pintura",
                        price: 2.50,
                        edited: false,
                        calculated: false,
                        type: 0
                    },
                    {
                        name: "Custo de instalação",
                        price: 20.00,
                        edited: false,
                        calculated: false,
                        type: 1
                    },
                    {
                        name: "Custo de instalação 2",
                        price: 0.00,
                        edited: false,
                        calculated: false,
                        type: 1
                    }
                ],
                accessories: [
                    {
                        id: 1,
                        name: "Silicone",
                        quantity: 0,
                        taxe: true,
                        print: true,
                        price: 0.00
                    },
                    {
                        id: 2,
                        name: "Sikadur",
                        quantity: 0,
                        taxe: true,
                        print: true,
                        price: 0.00
                    },
                    {
                        id: 3,
                        name: "Chumbador",
                        quantity: 0,
                        taxe: true,
                        print: true,
                        price: 0.00
                    },
                    {
                        id: 4,
                        name: "Parafuso",
                        quantity: 0,
                        taxe: true,
                        print: true,
                        price: 0.00
                    },
                    {
                        id: 5,
                        name: "Fita",
                        quantity: 0,
                        taxe: true,
                        print: true,
                        price: 0.00
                    },
                    {
                        id: 6,
                        name: "Guarnição",
                        quantity: 0,
                        taxe: true,
                        print: true,
                        price: 0.00
                    },
                    {
                        id: 7,
                        name: "Vulcanização",
                        quantity: 0,
                        taxe: true,
                        print: true,
                        price: 0.00
                    },
                    {
                        id: 8,
                        name: "Curvatura",
                        quantity: 0,
                        taxe: true,
                        print: true,
                        price: 0.00
                    },
                    {
                        id: 9,
                        name: "Chapas",
                        quantity: 0,
                        taxe: true,
                        print: true,
                        price: 0.00
                    },
                    {
                        id: 10,
                        name: "Grelhas",
                        quantity: 0,
                        taxe: true,
                        print: true,
                        price: 0.00
                    },
                    {
                        id: 11,
                        name: "Routiamento de ACM",
                        quantity: 0,
                        taxe: true,
                        print: true,
                        price: 0.00
                    }
                ],
                deliveries: [
                    {
                        name: "Des. Viagem Instalação",
                        quantity: 0,
                        price: 0.00
                    },
                    {
                        name: "Des. Viagem Gerência",
                        quantity: 0,
                        price: 0.00
                    },
                    {
                        name: "Des. Viagem Diretoria",
                        quantity: 0,
                        price: 0.00
                    },
                    {
                        name: "Reg. de Empreiteiros",
                        quantity: 0,
                        price: 0.00
                    },
                    {
                        name: "Outros",
                        quantity: 0,
                        price: 0.00
                    }
                ]
            },
            created: function () {

                this.products.push({
                    name: "Custo fixo",
                    price: this.linePrice,
                    edited: false,
                    calculated: false,
                    type: 0
                });
                this.products.push({
                    name: "Custo dos acessórios",
                    price: 0.00,
                    edited: false,
                    calculated: true,
                    calculatedValue: 'calcTotalAccessories',
                    type: 0
                });

                this.accessories.push({
                    id: "others1",
                    name: "Outros 1",
                    quantity: 0,
                    taxe: true,
                    print: false,
                    price: 0.00
                });
                this.accessories.push({
                    id: "others2",
                    name: "Outros 2",
                    quantity: 0,
                    taxe: true,
                    print: false,
                    price: 0.00
                });

                this.calculated = {
                    delivery: {
                        name: "Frete Hospedagem Passagem",
                        calculatedValue: "calcTotalDeliveries"
                    },
                    commission: {
                        name: "Comissão",
                        percent: 0.0,
                        calculatedValue: "calcCommissionTotal"
                    },
                    taxes: {
                        name: "Impostos",
                        rate: 100.0,
                        percent: 11.0,
                        calculatedValue: "calcTaxesTotal"
                    },
                    variable: {
                        name: "Custo Variável Total",
                        calculatedValue: "calcVariableTotal"
                    },
                    total: {
                        name: "Custo Total",
                        calculatedValue: "calcTotalTotal"
                    },
                    sell: {
                        name: "Preço de Venda",
                        calculatedValue: "calcSellTotal"
                    },
                    taxes_margin: {
                        name: "Margem de contribuição",
                        calculatedValue: "calcMarginTotal",
                        calculatedValue2: "calcMarginPercent"
                    },
                    kiloPrice: {
                        name: "Preço Kilo",
                        value: this.kiloPrice
                    },
                    profit: {
                        name: "Lucro",
                        calculatedValue: "calcProfitTotal",
                        calculatedValue2: "calcProfitPercent"
                    }
                }
            },
            watch: {
                kiloPrice: {
                    handler: function () {
                        this.calculated.kiloPrice.value = this.kiloPrice;
                    },
                    deep: true
                },
                linePrice: {
                    handler: function () {
                        this.products[this.products.length - 2].price = this.linePrice;
                    },
                    deep: true
                }
            }
        })
    });

    floatingBox.updateData();

    bindRemoveAccessories(id);
}

var floatingBox = null;
function drawFloatingBox () {
    floatingBox = new Vue({
        el: '.box-total',
        delimiters: ["((", "))"],
        data: {
            total: 0.0,
            sell: 0.0,
            profit: 0.0,
            profit2: 0.0
        },
        methods: {
            updateData: function () {
                this.total = normalizeFloat(this.sumTotalCalculatedValue());
                this.sell = normalizeFloat(this.sumSellCalculatedValue());
                this.profit = normalizeFloat(this.sumProfitCalculatedValue());
                this.profit2 = normalizeFloat(this.sumProfitCalculatedValue2());
            },
            sumTotalCalculatedValue: function () {
                var total = 0.0, calc;
                for (var i in calculators) {
                    if (calculators.hasOwnProperty(i)) {
                        calc = calculators[i].calculator;

                        total += calc.call(calc.calculated.total.calculatedValue);
                    }
                }

                return total;
            },
            sumSellCalculatedValue: function () {
                var total = 0.0, calc;
                for (var i in calculators) {
                    if (calculators.hasOwnProperty(i)) {
                        calc = calculators[i].calculator;

                        total += calc.call(calc.calculated.sell.calculatedValue);
                    }
                }

                return total;
            },
            sumProfitCalculatedValue: function () {
                var total = 0.0, calc;
                for (var i in calculators) {
                    if (calculators.hasOwnProperty(i)) {
                        calc = calculators[i].calculator;

                        total += calc.call(calc.calculated.profit.calculatedValue);
                    }
                }

                return total;
            },
            sumProfitCalculatedValue2: function () {
                var total = 0.0, calc, count = 0;
                for (var i in calculators) {
                    if (calculators.hasOwnProperty(i)) {
                        calc = calculators[i].calculator;
                        count++;

                        total += calc.call(calc.calculated.profit.calculatedValue2);
                    }
                }

                return total / count;
            }
        }
    });
}