var constants = {};
var calculators = [];

function cloneArrayOfObjects(arr) {
    var temp = [];

    for (var i = 0; i < arr.length; i++) {
        temp[i] = cloneObject(arr[i]);
    }

    return temp;
}

function cloneObject(obj) {
    if (obj === null || typeof obj !== 'object') {
        return obj;
    }

    var temp = obj.constructor(); // give temp the original obj's constructor
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            temp[key] = cloneObject(obj[key]);
        }
    }

    return temp;
}

function normalizeFloat(value) {
    value = value * 1.0;
    return (value.toFixed(2) * 1.0);
}

function initCalculator(kiloPrice) {
    constants.kiloPrice = kiloPrice;
    constants.lines = [];

    Object.freeze(constants);
}

function addLine(id, price, kilo, meters) {
    var newConstants = cloneObject(constants);

    newConstants.lines.push({
        id: id,
        price: price,
        kilo: kilo,
        meters: meters
    });

    constants = newConstants;

    Object.freeze(constants);

    addCalculator(id);
}

function getLine(id) {
    for (var i in constants.lines) {
        if (constants.lines[i].id == id) {
            return constants.lines[i];
        }
    }

    return null;
}

function getCalculator(id) {
    for (var i in calculators) {
        if (calculators[i].id == id) {
            return calculators[i].calculator;
        }
    }

    return null;
}

function bindRemoveAccessories(id) {
    $("#" + id + " a.btnMinus").unbind('click').bind('click', function (event) {
        event.preventDefault();

        var $tr = $(this).parent().parent();

        var accessoryId = $tr.find("input[type=hidden]").val();
        var calculator = getCalculator(id);
        var accessoryIndex = calculator.getAccessoryIndex(accessoryId);

        calculator.accessories.splice(accessoryIndex, 1);
    });
}

function newLine(clickedElement) {
    var $that = $(clickedElement);
    var $parent = $that.parent().parent();

    $.post(
        $that.attr('data-route'),
        {
            line_id: $parent.find('[name=lines]').val(),
            type: $parent.find('[name=calculator_type]').val(),
            meters: $parent.find('[name=line_meters]').val(),
            kilo: $parent.find('[name=line_kilo]').val()
        },
        function (data) {
            if (data.error) {
                swal({
                    title: "Erro ao inserir linha",
                    text: "Algum erro ocorreu ao tentar inserir esta linha na calculadora. Entre em contato com o suporte.",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
                return false;
            }

            $('#calculators').append(data.contents);
            addLine(data.id, data.price, data.kilo, data.meters);
        }
    );
}

$(function () {
    $('#addLine').bind('click', function (e) {
        e.preventDefault();
        newLine(this);
    });
});

$(function () {

    /* ------------------------------------------------------------------------------
     *
     *  # Form Layouts
     *
     * ---------------------------------------------------------------------------- */

    // Basic
    $('.select').select2();


    //
    // Select with icons
    //

    // Initialize
    $(".select-icons").select2({
        formatResult: iconFormat,
        minimumResultsForSearch: "-1",
        width: '100%',
        formatSelection: iconFormat,
        escapeMarkup: function (m) {
            return m;
        }
    });

    // Format icons
    function iconFormat(state) {
        var originalOption = state.element;
        return "<i class='icon-" + $(originalOption).data('icon') + "'></i>" + state.text;
    }

    // Styled form components
    // ------------------------------

    // Checkboxes, radios
    $(".styled").uniform({radioClass: 'choice'});

    // File input
    $(".file-styled").uniform({
        fileButtonHtml: '<i class="icon-googleplus5"></i>',
        wrapperClass: 'bg-warning'
    });


    /* ------------------------------------------------------------------------------
     *
     *  # Modais
     *
     * ---------------------------------------------------------------------------- */

    // ALerts e Modais de Confirmação
    // ------------------------------

    // Salvar
    $('.salvar').on('click', function () {
        swal({
                title: "Tem certeza que deseja salvar?",
                text: "As alterações não poderão ser desfeitas!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#EF5350",
                confirmButtonText: "Sim, desejo salvar!",
                cancelButtonText: "Não, cancele por favor!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    swal({
                        title: "Salvo!",
                        text: "Registro salvo com sucesso!",
                        confirmButtonColor: "#66BB6A",
                        type: "success"
                    });
                }
                else {
                    swal({
                        title: "Cancelado",
                        text: "Esse registro não foi salvo.",
                        confirmButtonColor: "#2196F3",
                        type: "error"
                    });
                }
            });
    });
    // Fechar
    $('.fechar').on('click', function () {
        swal({
                title: "Tem certeza que deseja fechar?",
                text: "As alterações não salvas serão perdidas!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#EF5350",
                confirmButtonText: "Sim, sair dessa tela!",
                cancelButtonText: "Não, cancele por favor!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    window.location = "index.html"
                }
                else {
                    swal({
                        title: "Cancelado",
                        text: "Você permanecerá na tela.",
                        confirmButtonColor: "#2196F3",
                        type: "error"
                    });
                }
            });
    });
});
