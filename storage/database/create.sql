CREATE SCHEMA almais;
USE almais;

CREATE TABLE `lines` (
  id         INT(11) UNSIGNED                               NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name       VARCHAR(255)                                   NOT NULL,
  price_buy  DECIMAL(10, 2) DEFAULT 0.00                    NOT NULL,
  price_sell DECIMAL(10, 2) DEFAULT 0.00                    NOT NULL,
  active     TINYINT(1) DEFAULT 1                           NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP            NOT NULL,
  updated_at TIMESTAMP                                      NULL,
  deleted_at TIMESTAMP                                      NULL
);

CREATE TABLE `products_type` (
  id         INT(11) UNSIGNED                               NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name       VARCHAR(255)                                   NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP            NOT NULL,
  updated_at TIMESTAMP                                      NULL,
  deleted_at TIMESTAMP                                      NULL
);
INSERT INTO products_type (id, name, created_at, updated_at, deleted_at) VALUES (1, 'Produto', '2016-12-14 10:19:13', null, null);
INSERT INTO products_type (id, name, created_at, updated_at, deleted_at) VALUES (2, 'Acessório', '2016-12-14 10:19:13', null, null);

CREATE TABLE `products` (
  id               INT(11) UNSIGNED                               NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name             VARCHAR(255)                                   NOT NULL,
  products_type_id INT(11) UNSIGNED                               NOT NULL,
  price_buy        DECIMAL(10, 2) DEFAULT 0.00                    NOT NULL,
  price_sell       DECIMAL(10, 2) DEFAULT 0.00                    NOT NULL,
  active           TINYINT(1) DEFAULT 1                           NOT NULL,
  created_at       TIMESTAMP DEFAULT CURRENT_TIMESTAMP            NOT NULL,
  updated_at       TIMESTAMP                                      NULL,
  deleted_at       TIMESTAMP                                      NULL,
  CONSTRAINT fk_products_products_type FOREIGN KEY (products_type_id)
  REFERENCES products_type (id)
);
INSERT INTO products (id, name, products_type_id, price_sell) VALUES (1, 'Alumínio', 1, 74.19);

CREATE TABLE `people` (
  id         INT(11) UNSIGNED                               NOT NULL PRIMARY KEY AUTO_INCREMENT,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP            NOT NULL,
  updated_at TIMESTAMP                                      NULL,
  deleted_at TIMESTAMP                                      NULL
);

CREATE TABLE `building_works` (
  id         INT(11) UNSIGNED                               NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name       VARCHAR(255)                                   NOT NULL,
  people_id  INT(11) UNSIGNED                               NOT NULL,
  active     TINYINT(1) DEFAULT 1                           NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP            NOT NULL,
  updated_at TIMESTAMP                                      NULL,
  deleted_at TIMESTAMP                                      NULL,
  CONSTRAINT fk_building_works_people FOREIGN KEY (people_id)
  REFERENCES people (id)
);

ALTER TABLE people
  ADD COLUMN name VARCHAR(255) NOT NULL
  AFTER id;

ALTER TABLE people
  ADD COLUMN identifyer VARCHAR(100) NOT NULL
  AFTER `name`
,
  ADD COLUMN email VARCHAR(255) NOT NULL
  AFTER `identifyer`
,
  ADD COLUMN phone1 VARCHAR(50) NOT NULL
  AFTER `email`
,
  ADD COLUMN phone2 VARCHAR(50) NOT NULL
  AFTER `phone1`
,
  ADD COLUMN zip_code VARCHAR(100) NOT NULL
  AFTER `phone2`
,
  ADD COLUMN address VARCHAR(255) NOT NULL
  AFTER `zip_code`
,
  ADD COLUMN number VARCHAR(100) NOT NULL
  AFTER `address`
,
  ADD COLUMN address_extra VARCHAR(100) NOT NULL
  AFTER `number`
,
  ADD COLUMN city VARCHAR(255) NOT NULL
  AFTER `address_extra`
,
  ADD COLUMN state VARCHAR(10) NOT NULL
  AFTER `city`
,
  ADD COLUMN active TINYINT(1) DEFAULT 1 NOT NULL
  AFTER state;

ALTER TABLE people
  CHANGE COLUMN number address_number VARCHAR(100) NOT NULL
  AFTER `address`;

ALTER TABLE people
  CHANGE COLUMN identifyer identifier VARCHAR(100) NOT NULL
  AFTER `name`;