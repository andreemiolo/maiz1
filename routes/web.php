<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('cerebelo.index');
})->name('cerebelo.index');;

Route::get('/registro-visitas', function () {
    return view('cerebelo.registro-visitas');
})->name('cerebelo.registro-visitas');;

Route::get('/usuarios-bloqueados', function () {
    return view('cerebelo.usuarios-bloqueados');
})->name('cerebelo.usuarios-bloqueados');;

Route::get('/financeiro', function () {
    return view('cerebelo.financeiro');
})->name('cerebelo.financeiro');;

Route::get('/index-interna', function () {
    return view('cerebelo.index-interna');
})->name('cerebelo.index-interna');;

Route::get('/administrar-aplicativo', function () {
    return view('cerebelo.administrar-aplicativo');
})->name('cerebelo.administrar-aplicativo');;

Route::get('/cadastro-alunos', function () {
    return view('cerebelo.cadastro-alunos');
})->name('cerebelo.cadastro-alunos');;

Route::get('/cadastro-parceiros', function () {
    return view('cerebelo.cadastro-parceiros');
})->name('cerebelo.cadastro-parceiros');;

Route::get('/relatorios', function () {
    return view('cerebelo.relatorios');
})->name('cerebelo.relatorios');;

Route::get('/transacoes', function () {
    return view('cerebelo.transacoes');
})->name('cerebelo.transacoes');;

Route::get('/adicionar-parceiro', function () {
    return view('cerebelo.adicionar-parceiro');
})->name('cerebelo.adicionar-parceiro');;

Route::get('/adicionar-aluno', function () {
    return view('cerebelo.adicionar-aluno');
})->name('cerebelo.adicionar-aluno');;

Route::get('/mensagens', function () {
    return view('cerebelo.mensagens');
})->name('cerebelo.mensagens');;

Route::get('/retiradas', function () {
    return view('cerebelo.retiradas');
})->name('cerebelo.retiradas');;

Route::get('/tabela-retiradas', function () {
    return view('cerebelo.tabela-retiradas');
})->name('cerebelo.tabela-retiradas');;