$(function(){function e(){var e=$(window).height()-$("body > .navbar").outerHeight()-$("body > .navbar + .navbar").outerHeight()-$("body > .navbar + .navbar-collapse").outerHeight();$(".page-container").attr("style","min-height:"+e+"px")}$(".panel-heading, .page-header-content, .panel-body").has("> .heading-elements").append('<a class="heading-elements-toggle"><i class="icon-menu"></i></a>'),$(".heading-elements-toggle").on("click",function(){$(this).parent().children(".heading-elements").toggleClass("visible")}),$(".breadcrumb-line").has(".breadcrumb-elements").append('<a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>'),$(".breadcrumb-elements-toggle").on("click",function(){$(this).parent().children(".breadcrumb-elements").toggleClass("visible")}),$(document).on("click",".dropdown-content",function(e){e.stopPropagation()}),$(".navbar-nav .disabled a").on("click",function(e){e.preventDefault(),e.stopPropagation()}),$('.dropdown-content a[data-toggle="tab"]').on("click",function(){$(this).tab("show")}),$(".panel [data-action=reload]").click(function(e){e.preventDefault();var a=$(this).parent().parent().parent().parent().parent();$(a).block({message:'<i class="icon-spinner2 spinner"></i>',overlayCSS:{backgroundColor:"#fff",opacity:.8,cursor:"wait","box-shadow":"0 0 0 1px #ddd"},css:{border:0,padding:0,backgroundColor:"none"}}),window.setTimeout(function(){$(a).unblock()},2e3)}),$(".category-title [data-action=reload]").click(function(e){e.preventDefault();var a=$(this).parent().parent().parent().parent();$(a).block({message:'<i class="icon-spinner2 spinner"></i>',overlayCSS:{backgroundColor:"#000",opacity:.5,cursor:"wait","box-shadow":"0 0 0 1px #000"},css:{border:0,padding:0,backgroundColor:"none",color:"#fff"}}),window.setTimeout(function(){$(a).unblock()},2e3)}),$(".sidebar-default .category-title [data-action=reload]").click(function(e){e.preventDefault();var a=$(this).parent().parent().parent().parent();$(a).block({message:'<i class="icon-spinner2 spinner"></i>',overlayCSS:{backgroundColor:"#fff",opacity:.8,cursor:"wait","box-shadow":"0 0 0 1px #ddd"},css:{border:0,padding:0,backgroundColor:"none"}}),window.setTimeout(function(){$(a).unblock()},2e3)}),$(".category-collapsed").children(".category-content").hide(),$(".category-collapsed").find("[data-action=collapse]").addClass("rotate-180"),$(".category-title [data-action=collapse]").click(function(a){a.preventDefault();var i=$(this).parent().parent().parent().nextAll();$(this).parents(".category-title").toggleClass("category-collapsed"),$(this).toggleClass("rotate-180"),e(),i.slideToggle(150)}),$(".panel-collapsed").children(".panel-heading").nextAll().hide(),$(".panel-collapsed").find("[data-action=collapse]").children("i").addClass("rotate-180"),$(".panel [data-action=collapse]").click(function(a){a.preventDefault();var i=$(this).parent().parent().parent().parent().nextAll();$(this).parents(".panel").toggleClass("panel-collapsed"),$(this).toggleClass("rotate-180"),e(),i.slideToggle(150)}),$(".panel [data-action=close]").click(function(a){a.preventDefault();var i=$(this).parent().parent().parent().parent().parent();e(),i.slideUp(150,function(){$(this).remove()})}),$(".category-title [data-action=close]").click(function(a){a.preventDefault();var i=$(this).parent().parent().parent().parent();e(),i.slideUp(150,function(){$(this).remove()})}),$(".navigation").find("li.active").parents("li").addClass("active"),$(".navigation").find("li").not(".active, .category-title").has("ul").children("ul").addClass("hidden-ul"),$(".navigation").find("li").has("ul").children("a").addClass("has-ul"),$(".dropdown-menu:not(.dropdown-content), .dropdown-menu:not(.dropdown-content) .dropdown-submenu").has("li.active").addClass("active").parents(".navbar-nav .dropdown:not(.language-switch), .navbar-nav .dropup:not(.language-switch)").addClass("active"),$(".navigation-main > .navigation-header > i").tooltip({placement:"right",container:"body"}),$(".navigation-main").find("li").has("ul").children("a").on("click",function(e){e.preventDefault(),$(this).parent("li").not(".disabled").not($(".sidebar-xs").not(".sidebar-xs-indicator").find(".navigation-main").children("li")).toggleClass("active").children("ul").slideToggle(250),$(".navigation-main").hasClass("navigation-accordion")&&$(this).parent("li").not(".disabled").not($(".sidebar-xs").not(".sidebar-xs-indicator").find(".navigation-main").children("li")).siblings(":has(.has-ul)").removeClass("active").children("ul").slideUp(250)}),$(".navigation-alt").find("li").has("ul").children("a").on("click",function(e){e.preventDefault(),$(this).parent("li").not(".disabled").toggleClass("active").children("ul").slideToggle(200),$(".navigation-alt").hasClass("navigation-accordion")&&$(this).parent("li").not(".disabled").siblings(":has(.has-ul)").removeClass("active").children("ul").slideUp(200)}),$(".sidebar-main-toggle").on("click",function(e){e.preventDefault(),$("body").toggleClass("sidebar-xs")}),$(document).on("click",".navigation .disabled a",function(e){e.preventDefault()}),$(document).on("click",".sidebar-control",function(){e()}),$(document).on("click",".sidebar-main-hide",function(e){e.preventDefault(),$("body").toggleClass("sidebar-main-hidden")}),$(document).on("click",".sidebar-secondary-hide",function(e){e.preventDefault(),$("body").toggleClass("sidebar-secondary-hidden")}),$(document).on("click",".sidebar-detached-hide",function(e){e.preventDefault(),$("body").toggleClass("sidebar-detached-hidden")}),$(document).on("click",".sidebar-all-hide",function(e){e.preventDefault(),$("body").toggleClass("sidebar-all-hidden")}),$(document).on("click",".sidebar-opposite-toggle",function(e){e.preventDefault(),$("body").toggleClass("sidebar-opposite-visible"),$("body").hasClass("sidebar-opposite-visible")?($("body").addClass("sidebar-xs"),$(".navigation-main").children("li").children("ul").css("display","")):$("body").removeClass("sidebar-xs")}),$(document).on("click",".sidebar-opposite-main-hide",function(e){e.preventDefault(),$("body").toggleClass("sidebar-opposite-visible"),$("body").hasClass("sidebar-opposite-visible")?$("body").addClass("sidebar-main-hidden"):$("body").removeClass("sidebar-main-hidden")}),$(document).on("click",".sidebar-opposite-secondary-hide",function(e){e.preventDefault(),$("body").toggleClass("sidebar-opposite-visible"),$("body").hasClass("sidebar-opposite-visible")?$("body").addClass("sidebar-secondary-hidden"):$("body").removeClass("sidebar-secondary-hidden")}),$(document).on("click",".sidebar-opposite-hide",function(e){e.preventDefault(),$("body").toggleClass("sidebar-all-hidden"),$("body").hasClass("sidebar-all-hidden")?($("body").addClass("sidebar-opposite-visible"),$(".navigation-main").children("li").children("ul").css("display","")):$("body").removeClass("sidebar-opposite-visible")}),$(document).on("click",".sidebar-opposite-fix",function(e){e.preventDefault(),$("body").toggleClass("sidebar-opposite-visible")}),$(".sidebar-mobile-main-toggle").on("click",function(e){e.preventDefault(),$("body").toggleClass("sidebar-mobile-main").removeClass("sidebar-mobile-secondary sidebar-mobile-opposite sidebar-mobile-detached")}),$(".sidebar-mobile-secondary-toggle").on("click",function(e){e.preventDefault(),$("body").toggleClass("sidebar-mobile-secondary").removeClass("sidebar-mobile-main sidebar-mobile-opposite sidebar-mobile-detached")}),$(".sidebar-mobile-opposite-toggle").on("click",function(e){e.preventDefault(),$("body").toggleClass("sidebar-mobile-opposite").removeClass("sidebar-mobile-main sidebar-mobile-secondary sidebar-mobile-detached")}),$(".sidebar-mobile-detached-toggle").on("click",function(e){e.preventDefault(),$("body").toggleClass("sidebar-mobile-detached").removeClass("sidebar-mobile-main sidebar-mobile-secondary sidebar-mobile-opposite")}),$(window).on("resize",function(){setTimeout(function(){e(),$(window).width()<=768?($("body").addClass("sidebar-xs-indicator"),$(".sidebar-opposite").insertBefore(".content-wrapper"),$(".sidebar-detached").insertBefore(".content-wrapper")):($("body").removeClass("sidebar-xs-indicator"),$(".sidebar-opposite").insertAfter(".content-wrapper"),$("body").removeClass("sidebar-mobile-main sidebar-mobile-secondary sidebar-mobile-detached sidebar-mobile-opposite"),$("body").hasClass("has-detached-left")?$(".sidebar-detached").insertBefore(".container-detached"):$("body").hasClass("has-detached-right")&&$(".sidebar-detached").insertAfter(".container-detached"))},100)}).resize(),$('[data-popup="popover"]').popover(),$('[data-popup="tooltip"]').tooltip()});
/* ------------------------------------------------------------------------------
*
*  # Steps wizard
*
*  Specific JS code additions for wizard_steps.html page
*
*  Version: 1.0
*  Latest update: Aug 1, 2015
*
* ---------------------------------------------------------------------------- */

$(function() {


    // Wizard examples
    // ------------------------------

    // Basic wizard setup
    $(".steps-basic").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        titleTemplate: '<span class="number">#index#</span> #title#',
        labels: {
            finish: 'Submit'
        },
        onFinished: function (event, currentIndex) {
            alert("Form submitted.");
        }
    });


    // Async content loading
    $(".steps-async").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        titleTemplate: '<span class="number">#index#</span> #title#',
        labels: {
            finish: 'Submit'
        },
        onContentLoaded: function (event, currentIndex) {
            $(this).find('select.select').select2();

            $(this).find('select.select-simple').select2({
                minimumResultsForSearch: '-1'
            });

            $(this).find('.styled').uniform({
                radioClass: 'choice'
            });

            $(this).find('.file-styled').uniform({
                wrapperClass: 'bg-warning',
                fileButtonHtml: '<i class="icon-googleplus5"></i>'
            });
        },
        onFinished: function (event, currentIndex) {
            alert("Form submitted.");
        }
    });


    // Saving wizard state
    $(".steps-state-saving").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        saveState: true,
        titleTemplate: '<span class="number">#index#</span> #title#',
        autoFocus: true,
        onFinished: function (event, currentIndex) {
            alert("Form submitted.");
        }
    });


    // Specify custom starting step
    $(".steps-starting-step").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        startIndex: 0,
        titleTemplate: '<span class="number">#index#</span> #title#',
        autoFocus: true,
        onFinished: function (event, currentIndex) {
            alert("Form submitted.");
        }
    });


    //
    // Wizard with validation
    //

    // Show form
    var form = $(".steps-validation").show();


    // Initialize wizard
    $(".steps-validation").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        titleTemplate: '<span class="number">#index#</span> #title#',
        autoFocus: true,
        onStepChanging: function (event, currentIndex, newIndex) {

            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }

            // Forbid next action on "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age-2").val()) < 18) {
                return false;
            }

            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {

                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }

            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },

        onStepChanged: function (event, currentIndex, priorIndex) {

            // Used to skip the "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age-2").val()) >= 18) {
                form.steps("next");
            }

            // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3) {
                form.steps("previous");
            }
        },

        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },

        onFinished: function (event, currentIndex) {
            alert("Submitted!");
        }
    });


    // Initialize validation
    $(".steps-validation").validate({
        ignore: 'input[type=hidden], .select2-input',
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        errorPlacement: function(error, element) {
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }
            else {
                error.insertAfter(element);
            }
        },
        rules: {
            email: {
                email: true
            }
        }
    });



    // Initialize plugins
    // ------------------------------

    // Select2 selects
    $('.select').select2();


    // Simple select without search
    $('.select-simple').select2({
        minimumResultsForSearch: '-1'
    });


    // Styled checkboxes and radios
    $('.styled').uniform({
        radioClass: 'choice'
    });


    // Styled file input
    $('.file-styled').uniform({
        wrapperClass: 'bg-warning',
        fileButtonHtml: '<i class="icon-googleplus5"></i>'
    });
    
});

/* ------------------------------------------------------------------------------
 *
 *  # Basic datatables
 *
 *  Specific JS code additions for datatable_basic.html page
 *
 *  Version: 1.0
 *  Latest update: Aug 1, 2015
 *
 * ---------------------------------------------------------------------------- */

var tableSetup = function (route) {

// Override defaults
    // ------------------------------

    // Setting datatable defaults
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        columnDefs: [{
            orderable: false,
            width: '100px',
            targets: [4]
        }],
        dom: '<"datatable-header"fTl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });


    // Tabletools defaults
    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group DTTT_container", // buttons container
        "buttons": {
            "normal": "btn btn-default", // default button classes
            "disabled": "disabled" // disabled button classes
        },
        "collection": {
            "container": "dropdown-menu" // collection container to take dropdown menu styling
        },
        "select": {
            "row": "success" // selected row class
        }
    });


    // Table setup
    // ------------------------------


    // Individual column searching with text inputs
    $('.datatable-column-search-inputs tfoot td.input-filter').each(function () {
        var title = $('.datatable-column-search-inputs thead th').eq($(this).index()).text();
        $(this).html('<input type="text" class="form-control input-sm" placeholder="Search ' + title + '" />');
    });

    var table = $('.datatable-column-search-inputs').DataTable({
        tableTools: {
            sRowSelect: "os",
            aButtons: ["select_all", "select_none"]
        },
        ajax: route,
        "columns": [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'identifier', name: 'identifier'},
            {data: 'active', name: 'active'},
            {data: 'actions', name: 'actions', defaultContent: '-', orderable: false, searchable: false}
        ],
        drawCallback: function () {
            // Ativar
            $('.ativar').unbind('click').bind('click', function () {
                swal({
                        title: "Tem certeza deseja ativar esse registro?",
                        text: "Essa alteração pode afetar alguns relatórios!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#EF5350",
                        confirmButtonText: "Sim, ativar registro!",
                        cancelButtonText: "Não, cancele por favor!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            swal({
                                title: "Ativado!",
                                text: "Registro ativado com sucesso!",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            });
                        }
                        else {
                            swal({
                                title: "Cancelado",
                                text: "Esse registro está permanece inativo",
                                confirmButtonColor: "#2196F3",
                                type: "error"
                            });
                        }
                    });
            });
            // Inativar
            $('.inativar').unbind('click').bind('click', function () {
                swal({
                        title: "Tem certeza deseja inativar esse registro?",
                        text: "Essa alteração fará com que você não possa manipular esse registro em certas áreas!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#EF5350",
                        confirmButtonText: "Sim, inative isso!",
                        cancelButtonText: "Não, cancele por favor!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            swal({
                                title: "Inativado!",
                                text: "Registro inativado com sucesso!",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            });
                        }
                        else {
                            swal({
                                title: "Cancelado",
                                text: "Esse registro está ativo :)",
                                confirmButtonColor: "#2196F3",
                                type: "error"
                            });
                        }
                    });
            });
            // Excluir
            $('.excluir').unbind('click').bind('click', function () {
                swal({
                        title: "Tem certeza que deseja excluir?",
                        text: "Após a exclusão não será possível recuperar esses dados!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#EF5350",
                        confirmButtonText: "Sim, exclua isso!",
                        cancelButtonText: "Não, cancele por favor!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            swal({
                                title: "Excluído!",
                                text: "Registro excluído com sucesso!",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            });
                        }
                        else {
                            swal({
                                title: "Cancelado",
                                text: "Esse registro está seguro :)",
                                confirmButtonColor: "#2196F3",
                                type: "error"
                            });
                        }
                    });
            });
        },
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var select = $('<select class="filter-select select" data-placeholder="Filter"><option value=""></option></select>')
                    .appendTo($(column.footer()).not(':last-child').not(".input-filter").empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });

    table.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            that.search(this.value).draw();
        });
    });


    // External table additions
    // ------------------------------

    // Add placeholder to the datatable filter option
    $('.dataTables_filter input[type=search]').attr('placeholder', 'Type to filter...');


    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: "-1"
    });

    // Default initialization
    $('.select').select2({
        minimumResultsForSearch: "-1"
    });


    // ALerts e Modais de Confirmação
    // ------------------------------

    // Salvar
    $('.salvar').on('click', function (e) {
        e.preventDefault();

        swal({
                title: "Tem certeza que deseja salvar?",
                text: "As alterações não poderão ser desfeitas!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#EF5350",
                confirmButtonText: "Sim, desejo salvar!",
                cancelButtonText: "Não, cancele por favor!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    swal({
                        title: "Salvo!",
                        text: "Registro salvo com sucesso!",
                        confirmButtonColor: "#66BB6A",
                        type: "success"
                    });
                    $('form').submit();
                }
                else {
                    swal({
                        title: "Cancelado",
                        text: "Esse registro não foi salvo.",
                        confirmButtonColor: "#2196F3",
                        type: "error"
                    });
                }
            });
    });
};

//# sourceMappingURL=clients.js.map
