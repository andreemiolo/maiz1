<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'lines'], function () {
    Route::get('/{id?}', ['as' => 'cerebelo.line.index', 'uses' => 'LineController@index'])->where(['id' => '[0-9]*']);
    Route::post('/{id?}', ['as' => 'cerebelo.line.store', 'uses' => 'LineController@store'])->where(['id' => '[0-9]*']);
    Route::get('/datatables', ['as' => 'cerebelo.line.datatables', 'uses' => 'LineController@datatables']);
});

Route::group(['prefix' => 'products'], function () {
    Route::get('/{id?}', ['as' => 'cerebelo.products.index', 'uses' => 'ProductController@index'])->where(['id' => '[0-9]*']);
    Route::post('/{id?}', ['as' => 'cerebelo.products.store', 'uses' => 'ProductController@store'])->where(['id' => '[0-9]*']);
    Route::get('/datatables', ['as' => 'cerebelo.products.datatables', 'uses' => 'ProductController@datatables']);
});

Route::group(['prefix' => 'building-works'], function () {
    Route::get('/{id?}', ['as' => 'cerebelo.building-works.index', 'uses' => 'BuildingWorkController@index'])->where(['id' => '[0-9]*']);
    Route::post('/{id?}', ['as' => 'cerebelo.building-works.store', 'uses' => 'BuildingWorkController@store'])->where(['id' => '[0-9]*']);
    Route::get('/datatables', ['as' => 'cerebelo.building-works.datatables', 'uses' => 'BuildingWorkController@datatables']);
});
Route::group(['prefix' => 'budgets'], function () {
    Route::get('/{id?}', ['as' => 'cerebelo.building-works.budgets.index', 'uses' => 'BuildingWorkController@budgets'])->where(['id' => '[0-9]*']);
    Route::post('/{id?}', ['as' => 'cerebelo.building-works.budgets.store', 'uses' => 'BuildingWorkController@index'])->where(['id' => '[0-9]*']);
    Route::get('/datatables/{type?}', ['as' => 'cerebelo.building-works.budgets.datatables', 'uses' => 'BuildingWorkController@datatables']);
});

Route::group(['prefix' => 'calculator'], function () {
    Route::get('/{id?}', ['as' => 'cerebelo.calculator.budget', 'uses' => 'CalculatorController@calculator'])->where(['id' => '[0-9]*']);
    Route::post('/new', ['as' => 'cerebelo.calculator.new', 'uses' => 'CalculatorController@newCalculator']);
});
