<?php

namespace App\Domains\Sales\Models;

use App\Domains\Main\Models\Person;
use Illuminate\Database\Eloquent\Model as Eloquent;

class BuildingWork extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'people_id',
        'active',
    ];

    protected $appends = [
        'cost'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'building_works';

    /**
     * Retorna somente os ativos
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where("active", 1);
    }

    /**
     * Relacionamento com cliente
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Person::class, 'people_id');
    }

    /**
     * Busca os dados para um input
     *
     * @return array
     */
    public static function inputSelect()
    {
        return self::active()
            ->orderBy('name', 'asc')
            ->pluck('name', 'id')->all();
    }

    public function getCostAttribute()
    {
        return 0.00;
    }
}