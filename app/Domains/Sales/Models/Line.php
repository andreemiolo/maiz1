<?php

namespace App\Domains\Sales\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Line extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'price_buy',
        'price_sell',
        'active',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'lines';

    /**
     * Retorna somente os ativos
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where("active", 1);
    }

    /**
     * Busca os dados para um input
     *
     * @return array
     */
    public static function inputSelect()
    {
        return self::active()
            ->orderBy('name', 'asc')
            ->pluck('name', 'id')->all();
    }
}