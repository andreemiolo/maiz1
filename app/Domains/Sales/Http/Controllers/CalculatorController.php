<?php

namespace App\Domains\Sales\Http\Controllers;

use App\Domains\Main\Models\Person;
use App\Domains\Sales\Models\BuildingWork;
use App\Domains\Sales\Models\Line;
use App\Domains\Sales\Models\Product;
use App\Http\Controllers\CerebeloController;
use Illuminate\Http\Request;

class CalculatorController extends CerebeloController
{
    /**
     * @var BuildingWork
     */
    protected $buildingWork;

    /**
     * @var Person
     */
    protected $person;

    /**
     * @var Line
     */
    protected $line;

    /**
     * @var Product
     */
    protected $product;

    /**
     * CalculatorController constructor.
     * @param BuildingWork $buildingWork
     * @param Person $client
     * @param Line $line
     * @param Product $product
     */
    public function __construct(BuildingWork $buildingWork, Person $client, Line $line, Product $product)
    {
        $this->buildingWork = $buildingWork;
        $this->person = $client;
        $this->line = $line;
        $this->product = $product;
    }

    public function calculator($id = null)
    {
        $new = true;
        $buildingWork = $this->buildingWork->find($id);
        $client = new Person();
        if ($buildingWork) {
            $client = $buildingWork->client;
            $new = false;
        }

        $aluminium = $this->product->find(1);

        $buildingWorks = $this->buildingWork->inputSelect();
        $people = $this->person->inputSelect();

        $lines = $this->line->inputSelect();

        return view('cerebelo.calculator', [
            'new' => $new,
            'aluminium_price' => (float)$aluminium->price_sell,
            'buildingWork' => $buildingWork,
            'client' => $client,
            'buildingWorks' => $buildingWorks,
            'people' => $people,
            'lines' => $lines,
        ]);
    }

    public function newCalculator(Request $request)
    {
        $line_id = $request->get('line_id');
        $type = $request->get('type');
        $meters = $request->get('meters');
        $kilo = $request->get('kilo');

        $line = $this->line->find($line_id);
        if (!$line) {
            return $this->json(['error' => true]);
        }

        $file = file_get_contents(view('cerebelo.calculatorSkeleton')->getPath());

        $id = 'calculator-' . $line->id . '-' . uniqid();
        $contents = str_replace('calculatorID', $id, $file);

        $response = [
            'error' => false,
            'id' => $id,
            'type' => $type,
            'meters' => $meters,
            'kilo' => $kilo,
            'price' => $line->price_sell,
            'contents' => $contents,
        ];

        return $this->json($response);
    }
}