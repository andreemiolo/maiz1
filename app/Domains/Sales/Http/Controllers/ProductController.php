<?php

namespace App\Domains\Sales\Http\Controllers;

use App\Domains\Sales\Models\Product;
use App\Domains\Sales\Models\ProductType;
use App\Http\Controllers\CerebeloController;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;

class ProductController extends CerebeloController
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var ProductType
     */
    protected $productType;

    public function __construct(Product $product, ProductType $productType)
    {
        $this->product = $product;
        $this->productType = $productType;
    }

    public function index($id = null)
    {
        $product = $this->product->find($id);
        $productTypes = $this->productType->inputSelect();

        return view('cerebelo.products', ['model' => $product, 'productTypes' => $productTypes]);
    }

    public function store(Request $request)
    {
        $id = $request->get('id', null);

        $update = true;
        $product = $this->product->find($id);
        if (!$product) {
            $product = new Product();
            $update = false;
        }

        $product->fill($request->all());
        $saved = $product->save();

        if ($saved) {
            if ($update) {
                return $this->redirectAfterSuccess("Registro atualizado com sucesso", "products.index", $product->id);
            }
            return $this->redirectAfterSuccess("Registro inserido com sucesso", "products.index", $product->id);
        }

        return $this->redirectBackAfterError(trans('messages.error.crud'));
    }

    public function datatables()
    {
        $result = $this->product->select('*');

        return Datatables::of($result)
            ->addColumn('active', function ($object) {
                $active = '<span class="label label-success">Ativa</span>';
                if (!$object->active) {
                    $active = '<span class="label label-danger">Inativa</span>';
                }
                return $active;
            })
            ->addColumn('products_type_id', function ($object) {
                return $object->type->name;
            })
            ->addColumn('actions', function ($object) {
                return '<ul class="icons-list">
                            <li class="dropdown">
                                <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a class="editar text-info-600" href="' . route('cerebelo.products.index', $object->id ) . '">
                                            <i class="icon-pencil3"></i>
                                            Editar
                                        </a>
                                    </li>
                                    <li>
                                        <a class="' . ($object->active ? 'ina' : 'a') . 'tivar text-' . ($object->active ? 'slate' : 'green') . '-800" href="#">
                                            <i class="icon-' . ($object->active ? 'blocked' : 'checkmark') . '"></i>
                                            ' . ($object->active ? 'Ina' : 'A') . 'tivar
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>';
            })
            ->make(true);
    }
}