<?php

namespace App\Domains\Sales\Http\Controllers;

use App\Domains\Main\Models\Person;
use App\Domains\Sales\Models\BuildingWork;
use App\Http\Controllers\CerebeloController;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;

class BuildingWorkController extends CerebeloController
{
    /**
     * @var BuildingWork
     */
    protected $buildingWork;

    /**
     * @var Person
     */
    protected $person;

    public function __construct(BuildingWork $buildingWork, Person $person)
    {
        $this->buildingWork = $buildingWork;
        $this->person = $person;
    }

    public function index($id = null)
    {
        $buildingWork = $this->buildingWork->find($id);
        $people = $this->person->inputSelect();

        return view('cerebelo.building_works', ['model' => $buildingWork, 'people' => $people]);
    }

    public function budgets()
    {
        return view('cerebelo.budgets');
    }

    public function store(Request $request)
    {
        $id = $request->get('id', null);

        $update = true;
        $buildingWork = $this->buildingWork->find($id);
        if (!$buildingWork) {
            $buildingWork = new BuildingWork();
            $update = false;
        }

        $buildingWork->fill($request->all());
        $saved = $buildingWork->save();

        if ($saved) {
            if ($update) {
                return $this->redirectAfterSuccess("Registro atualizado com sucesso", "building-works.index", $buildingWork->id);
            }
            return $this->redirectAfterSuccess("Registro inserido com sucesso", "building-works.index", $buildingWork->id);
        }

        return $this->redirectBackAfterError(trans('messages.error.crud'));
    }

    public function datatables($type = null)
    {
        $result = $this->buildingWork->all();

        return Datatables::of($result)
            ->addColumn('active', function ($object) {
                $active = '<span class="label label-success">Ativa</span>';
                if (!$object->active) {
                    $active = '<span class="label label-danger">Inativa</span>';
                }
                return $active;
            })
            ->addColumn('people_id', function ($object) {
                return $object->client->name;
            })
            ->addColumn('actions', function ($object) use ($type) {
                if ($type == 'budget') {
                    return '<a class="text-info-600" href="' . route('cerebelo.calculator.budget', $object->id) . '" alt="Calculadora" title="Calculadora">
                                <i class="icon-calculator"></i>
                            </a>';
                }

                return '<ul class="icons-list">
                            <li class="dropdown">
                                <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a class="editar text-info-600" href="' . route('cerebelo.building-works.index', $object->id) . '">
                                            <i class="icon-pencil3"></i>
                                            Editar
                                        </a>
                                    </li>
                                    <li>
                                        <a class="' . ($object->active ? 'ina' : 'a') . 'tivar text-' . ($object->active ? 'slate' : 'green') . '-800" href="#">
                                            <i class="icon-' . ($object->active ? 'blocked' : 'checkmark') . '"></i>
                                            ' . ($object->active ? 'Ina' : 'A') . 'tivar
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>';
            })
            ->make(true);
    }
}