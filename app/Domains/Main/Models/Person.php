<?php

namespace App\Domains\Main\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Person extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'identifier',
//        'contact',
        'email',
        'phone1',
        'phone2',
        'zip_code',
        'address',
        'address_number',
        'address_extra',
        'city',
        'state',
        'active',
    ];

    protected static $states = [
        'pt_BR' => [
            'AC' => 'Acre',
            'AL' => 'Alagoas',
            'AP' => 'Amapá',
            'AM' => 'Amazonas',
            'BA' => 'Bahia',
            'CE' => 'Ceará',
            'DF' => 'Distrito Federal',
            'ES' => 'Espírito Santo',
            'GO' => 'Goiás',
            'MA' => 'Maranhão',
            'MT' => 'Mato Grosso',
            'MS' => 'Mato Grosso do Sul',
            'MG' => 'Minas Gerais',
            'PA' => 'Pará',
            'PB' => 'Paraíba',
            'PR' => 'Paraná',
            'PE' => 'Pernambuco',
            'PI' => 'Piauí',
            'RJ' => 'Rio de Janeiro',
            'RN' => 'Rio Grande do Norte',
            'RS' => 'Rio Grande do Sul',
            'RO' => 'Rondônia',
            'RR' => 'Roraima',
            'SC' => 'Santa Catarina',
            'SP' => 'São Paulo',
            'SE' => 'Sergipe',
            'TO' => 'Tocantins'
        ]
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'people';

    /**
     * Retorna somente os ativos
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where("active", 1);
    }

    /**
     * Busca os tipos de pessoas no sistema
     *
     * @return array
     */
    public static function inputSelect()
    {
        return self::active()
            ->orderBy('name', 'asc')
            ->pluck('name', 'id')->all();
    }

    public static function getStates()
    {
        return self::$states;
    }

    public static function getStatesLanguage($lang)
    {
        return self::getStates()[$lang];
    }
}