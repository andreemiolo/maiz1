<?php

namespace App\Domains\Main\Http\Controllers;

use App\Domains\Main\Models\Person;
use App\Http\Controllers\CerebeloController;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;

class ClientController extends CerebeloController
{
    /**
     * @var Person
     */
    protected $person;

    public function __construct(Person $person)
    {
        $this->person = $person;
    }

    public function index($id = null)
    {
        $model = $this->person->find($id);

        return view('cerebelo.clients', ['model' => $model, 'states' => $this->person->getStatesLanguage('pt_BR')]);
    }

    public function store(Request $request)
    {
        $id = $request->get('id', null);

        $update = true;
        $client = $this->person->find($id);
        if (!$client) {
            $client = new Person();
            $update = false;
        }

        $client->fill($request->all());
        $saved = $client->save();

        if ($saved) {
            if ($update) {
                return $this->redirectAfterSuccess("Registro atualizado com sucesso", "clients.index", $client->id);
            }
            return $this->redirectAfterSuccess("Registro inserido com sucesso", "clients.index", $client->id);
        }

        return $this->redirectBackAfterError(trans('messages.error.crud'));
    }

    public function datatables($type = null)
    {
        $result = $this->person->all();

        return Datatables::of($result)
            ->addColumn('active', function ($object) {
                $active = '<span class="label label-success">Ativa</span>';
                if (!$object->active) {
                    $active = '<span class="label label-danger">Inativa</span>';
                }
                return $active;
            })
            ->addColumn('actions', function ($object) use ($type) {
                return '<ul class="icons-list">
                            <li class="dropdown">
                                <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a class="editar text-info-600" href="' . route('cerebelo.clients.index', $object->id) . '">
                                            <i class="icon-pencil3"></i>
                                            Editar
                                        </a>
                                    </li>
                                    <li>
                                        <a class="' . ($object->active ? 'ina' : 'a') . 'tivar text-' . ($object->active ? 'slate' : 'green') . '-800" href="#">
                                            <i class="icon-' . ($object->active ? 'blocked' : 'checkmark') . '"></i>
                                            ' . ($object->active ? 'Ina' : 'A') . 'tivar
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>';
            })
            ->make(true);
    }
}