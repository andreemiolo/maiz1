<?php

namespace App\Http\Controllers;

class CerebeloController extends Controller
{
    protected $baseFolder = "cerebelo.";
    protected $variables = array();

    /**
     * @param $view
     * @param array $data
     * @return \Illuminate\Http\Response
     */
    protected function view($view, $data = [])
    {
        return response()->view($this->baseFolder . $view, $data);
    }

    protected function json(array $v)
    {
        return response()->json($v);
    }

    /**
     * @param $message
     * @param $route
     * @param array $routeParameters
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectAfterSuccess($message, $route, $routeParameters = [])
    {
        return redirect()->route($this->baseFolder . $route, $routeParameters)->with('success', $message);
    }

    protected function redirectBackAfterSuccess($message)
    {
        return redirect()->back()->with('success', $message);
    }

    protected function redirectAfterError($message, $route)
    {
        return redirect()->route($this->baseFolder . $route)->with('fail', $message);
    }

    protected function redirectBackAfterError($message)
    {
        return redirect()->back()->with('fail', $message);
    }
}