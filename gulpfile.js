var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
     mix.copy('resources/assets/css/cerebelo/icons/icomoon/fonts', 'public/css/cerebelo/fonts');


    /*
     |--------------------------------------------------------------------------
     | Core CSS files
     |--------------------------------------------------------------------------
     */

    /* Global stylesheets ----- */
    mix.styles([
        'cerebelo/icons/icomoon/styles.css',
        'cerebelo/bootstrap.css',
        'cerebelo/components.css',
        'cerebelo/core.css',
        'cerebelo/colors.css',
        'cerebelo/custom.css'
    ], 'public/css/cerebelo/layout.css');



    /*
    |--------------------------------------------------------------------------
    | Pages CSS files
    |--------------------------------------------------------------------------
    */

    /* Calculator styles ----- */

    mix.styles([
        'cerebelo/pnotify.custom.css'
    ], 'public/css/cerebelo/calculator.css');

    /*
    |--------------------------------------------------------------------------
    | Core JS files
    |--------------------------------------------------------------------------
    */

    /* Global scripts ----- */
    mix.scripts([
        'cerebelo/plugins/loaders/pace.min.js',
        'cerebelo/core/libraries/jquery.min.js',
        'cerebelo/core/libraries/bootstrap.min.js',
        'cerebelo/plugins/loaders/blockui.min.js'
    ], 'public/js/cerebelo/layout.js');

    /* Global vue scripts ----- */
    mix.scripts([
        '../../../node_modules/vue/dist/vue.js',
        '../../../node_modules/vue-resource/dist/vue-resource.js',
        'cerebelo/vue/menu.js'
    ], 'public/js/cerebelo/vue/vue.js');

    /* Group scripts ----- */
    mix.scripts([
        'cerebelo/plugins/tables/datatables/datatables.min.js',
        'cerebelo/plugins/tables/datatables/extensions/tools.min.js',
        'cerebelo/plugins/forms/selects/select2.min.js',
        'cerebelo/plugins/notifications/bootbox.min.js',
        'cerebelo/plugins/notifications/sweet_alert.min.js'
    ], 'public/js/cerebelo/lists.js');

    /* Group scripts ----- */
    mix.scripts([
        'cerebelo/plugins/forms/wizards/steps.min.js',
        'cerebelo/plugins/forms/selects/select2.min.js',
        'cerebelo/plugins/forms/styling/uniform.min.js',
        'cerebelo/core/libraries/jasny_bootstrap.min.js',
        'cerebelo/plugins/forms/validation/validate.min.js',
        'cerebelo/plugins/extensions/cookie.js',
        'cerebelo/plugins/notifications/bootbox.min.js',
        'cerebelo/plugins/notifications/sweet_alert.min.js'
    ], 'public/js/cerebelo/forms.js');

    /*
    |--------------------------------------------------------------------------
    | Pages JS files
    |--------------------------------------------------------------------------
    */
    // 'cerebelo/vue/calculator.js'

    /* Dashboard scripts ----- */
    mix.scripts([
        'cerebelo/plugins/forms/selects/bootstrap_multiselect.js',
        'cerebelo/plugins/forms/styling/switchery.min.js',
        'cerebelo/plugins/forms/styling/uniform.min.js',
        'cerebelo/plugins/ui/moment/moment.min.js',
        'cerebelo/plugins/pickers/daterangepicker.js',
        'cerebelo/plugins/visualization/d3/d3.min.js',
        'cerebelo/plugins/visualization/d3/d3_tooltip.js',
        'cerebelo/core/app.js',
        'cerebelo/pages/dashboard.js'
    ], 'public/js/cerebelo/pages/dashboard.js');


    /* Estimate scripts ----- */
    mix.scripts([
        'cerebelo/core/app.js',
        'cerebelo/pages/budgets.js'
    ], 'public/js/cerebelo/pages/budgets.js');

    /* Line scripts ----- */
    mix.scripts([
        'cerebelo/core/app.js',
        'cerebelo/pages/line.js'
    ], 'public/js/cerebelo/pages/line.js');


    /* Products and accessories scripts ----- */
    mix.scripts([
        'cerebelo/core/app.js',
        'cerebelo/pages/products.js'
    ], 'public/js/cerebelo/pages/products.js');

    /* Clients scripts ----- */
    mix.scripts([
        'cerebelo/core/app.js',
        'cerebelo/pages/wizard_steps.js',
        'cerebelo/pages/clients.js'
    ], 'public/js/cerebelo/pages/clients.js');

    /* Constructions scripts ----- */
    mix.scripts([
        'cerebelo/core/app.js',
        'cerebelo/pages/wizard_steps.js',
        'cerebelo/pages/building_works.js'
    ], 'public/js/cerebelo/pages/building_works.js');

    /* Calculator scripts ----- */
    mix.scripts([
        'cerebelo/core/app.js',
        'cerebelo/plugins/ui/pnotify.custom.js',
        'cerebelo/pages/calculator.js',
        'cerebelo/vue/pages/calculator.js'
    ], 'public/js/cerebelo/pages/calculator.js');

});